﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace SWE_BASpiel
{
    class Task
    {
        private String frage;
        private String antwortA;
        private String antwortB;
        private String antwortC;
        private int richtig;

        public Task(String frg, String antA, String antB, String antC, int right)
        {
            frage = frg;
            antwortA = antA;
            antwortB = antB;
            antwortC = antC;
            richtig = right;
        }
        public String Frage
        {
            get { return frage; }
        }

        public String Antwort_1
        {
            get { return antwortA; }
        }

        public String Antwort_2
        {
            get { return antwortB; }
        }

        public String Antwort_3
        {
            get { return antwortC; }
        }

        public int Richtig
        {
            get { return richtig; }
        }

        

    }

    class Pruefung
    {
        private String name;
        private ArrayList fragen;

        public Pruefung(String name)
        {
            this.name = name;
            fragen = new ArrayList();
        }

        public void addTask(Task t)
        {
            this.fragen.Add(t);
        }

        public void addTask(ArrayList list)
        {
            foreach(Task t in list)
            {
                this.fragen.Add(t);
            }
        }

        public String Name
        {
            get { return name; }
        }

        public ArrayList Aufgaben
        {
            get { return fragen; }
        } 
    }

}
