﻿using System;
using System.Collections.ObjectModel;
using System.Windows;

namespace SWE_BASpiel
{
    /// <summary>
    /// Interaktionslogik für StartAuswahl.xaml
    /// </summary>
    public partial class StartAuswahl
    {
        public ObservableCollection<string> FirmenCollection;
        public Data Data;
        private readonly Company _firma;
        private bool beenden;

        public StartAuswahl()
        {
            InitializeComponent();
            Data = new Data();
            _firma = new Company();
            FirmenCollection = new ObservableCollection<string> { "SysThem", "Simons", "WoMaPhone" };
            CbFirma.ItemsSource = FirmenCollection;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (!String.IsNullOrEmpty(TxtName.Text) && !String.IsNullOrEmpty(TxtVorname.Text) && CbFirma.SelectedItem != null)
            {
                Data.Name = TxtName.Text;
                Data.Vorname = TxtVorname.Text;
                Data.Vorkurs = CheckboxVorkurs.IsChecked.Value;
                Data.Firma = _firma;

                beenden = true;

                MainWindow _main = new MainWindow();
                Application.Current.MainWindow = _main;
                _main.Data = Data;
                Close();
                _main.Show();
                _main.UpdateDefaultGui();
            }
            else
                MessageBox.Show("Deine Eingabe ist unvollständig", "Achtung", MessageBoxButton.OK, MessageBoxImage.Warning);
        }

        private void BtnVorkurs_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Der Vorkurs kostet dich etwas Kraft,\nhilft dir aber im Verlauf deines Studiums.\n...vielleicht :)", "Vorkurs", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        private void CbFirma_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            string _selectedCompany = CbFirma.SelectedItem.ToString();
            LbInformation.Items.Clear();

            switch (_selectedCompany)
            {
                case "SysThem":
                    _firma.Name = "SysThem";
                    _firma.LohnFaktor = 1;
                    _firma.StressFaktor = 1;
                    LbInformation.Items.Add(_firma.Name + " ist ein Systemintegrationsfirma, die aus einem\nFamilienbetrieb hervorgegangen ist.\nDas Arbeitsklima ist sehr entspannt, jedoch ist der\nLohn eher gering.");
                    break;
                case "Simons":
                    _firma.Name = "Simons";
                    _firma.LohnFaktor = 3;
                    _firma.StressFaktor = 3;
                    LbInformation.Items.Add(_firma.Name + " zählt weltweit zu den größten Unternehmen der\nAutomatisierungs- und Elektrotechnik.\nDie Ansprüch sind hoch, aber das spiegelt sich\nin der guten Bezahlung.");
                    break;
                case "WoMaPhone":
                    _firma.Name = "WoMaPhone";
                    _firma.LohnFaktor = 2;
                    _firma.StressFaktor = 2;
                    LbInformation.Items.Add("Die Firma " + _firma.Name + " ist im Bereich der\nTelekommunikation in mehreren Länder groß aufgestellt.\nDie Bezahlung und die Ansprüche an die Mitarbeiter\nsind gut abgewogen.");
                    break;
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!beenden && MessageBox.Show("Möchtest du das Spiel beenden?", "BA-Spiel", MessageBoxButton.YesNo, MessageBoxImage.Warning) != MessageBoxResult.Yes)
            {
                e.Cancel = true;
            }
        }
    }
}
