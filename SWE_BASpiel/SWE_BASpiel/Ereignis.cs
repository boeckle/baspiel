﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SWE_BASpiel
{
    class Ereignis
    {
        private String text;
        private int levelPlus;

        public Ereignis(String txt, int lvlPlus)
        {
            text = txt;
            levelPlus = lvlPlus;
        }

        public String Text
        {
            get { return text; }
        }

        public int LevelPush
        {
            get { return levelPlus; }
            set { levelPlus = value; }
        }
    }
}
