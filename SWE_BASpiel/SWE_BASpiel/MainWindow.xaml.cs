﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace SWE_BASpiel
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        //Hilfsvariablen und Figur
        public Rectangle Rec;
        int _zeile = 10, _spalte, _stepsleft;
        private readonly Spieler _player;
        public bool Rigth, Fertsch;
        public Data Data;
        private Data_Ereignis EreignisData;

        public MainWindow()
        {
            InitializeComponent();
            EreignisData = new Data_Ereignis();
            EreignisData.init();

            _player = new Spieler();
            Rec = new Rectangle
            {
                Stroke = Brushes.Black,
                Fill = new ImageBrush { ImageSource = new BitmapImage(new Uri(@"pack://application:,,,/SWE_BASpiel;component/Bilder/smileyface.jpg")), Stretch = Stretch.Uniform }
            };

            Grid.SetColumn(Rec, 0);
            Grid.SetRow(Rec, 10);
            MyGrid.Children.Add(Rec);
        }

        public void UpdateDefaultGui()
        {
            if (Data != null)
            {
                DataContext = Data;
                LblFirma.Content = Data.Firma.Name;
                LblStudent.Content = Data.Vorname + " " + Data.Name;
                Data.Lebensanzeige = 100;
                if (Data.Vorkurs)
                {
                    Data.Lebensanzeige -= 15;
                    Data.VorkursHinweis = true;
                }
                Data.AktuellesSemester = 1;
                LblSemester.Content = Data.AktuellesSemester + ". Semester";
            }
        }

        /// <summary>
        /// Anzahl der zu machenden Schritte würfeln
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnThrowDice_Click(object sender, RoutedEventArgs e)
        {
            BtnThrowDice.IsEnabled = false;
            //Würfeln simmulieren und Ergebnis anzeigen
            Random _rnd = new Random();
            _stepsleft = _rnd.Next(1, 7);
            TxtResult.Text = _stepsleft.ToString();

            //Schrittkette mit den zu setzenden Schritten
            do
            {
                Move(_player.CurrentState, _stepsleft);
            } while (_stepsleft != 0);

            BtnThrowDice.UpdateLayout();
            AllowUIToUpdate();
            BtnThrowDice.IsEnabled = true;

            CheckForEvent(_zeile, _spalte);
        }

        private void CheckForEvent(int zeile, int spalte)
        {
            Random _rnd = new Random();
            Ereignis[,] _allEvents = EreignisData.EreignisArray;

            if ((Ereignis)_allEvents[zeile, spalte] != null)
            {
                Ereignis _currEvent = _allEvents[zeile, spalte];
                MessageBox.Show(_currEvent.Text, "Ereignis", MessageBoxButton.OK, MessageBoxImage.Information);

                if (_currEvent.LevelPush == 0)
                    _currEvent.LevelPush = Data.Firma.LohnFaktor;
                if (_currEvent.LevelPush > 0)
                    Data.Lebensanzeige += (_currEvent.LevelPush * 2);
                else
                    Data.Lebensanzeige += (_currEvent.LevelPush * _rnd.Next(1,11));
                if (Data.Lebensanzeige > 100)
                    Data.Lebensanzeige = 100;
            }
        }

        /// <summary>
        /// Bewegen der Figur
        /// </summary>
        /// <param name="currentState">in welche Richtung sich die Figur bewegen soll</param>
        /// <param name="counter">Anzahl der übrigen Schritte</param>
        private void Move(State currentState, int counter)
        {
            switch (currentState)
            {
                case State.Up:                          //hoch bewegen
                    //auf roten Felder soll Figur stehen bleiben, bis Prüfung vorbei ist
                    if (_player.Y > 0)
                    {
                        //restliche Schritte
                        _stepsleft = counter - (2 - _player.Y);
                        _player.Y = 2 - _player.Y;
                        MoveFigurUp(_zeile - _player.Y);
                        _zeile -= _player.Y;
                        if (Rigth)
                        {
                            _player.CurrentState = State.Right;
                            Rigth = false;
                        }
                        else
                        {
                            _player.CurrentState = State.Left;
                            Rigth = true;
                        }
                        _player.Y = 0;
                    }
                    else
                    {
                        _player.Y = 1;
                        _stepsleft = 0;
                        _zeile -= _player.Y;
                        MoveFigurUp(_zeile);
                        Prüfung();
                    }
                    break;

                case State.Right:                       //nach rechts bewegen
                    if (_player.X + counter < 7)
                    {
                        _player.X += counter;
                        _spalte = _player.X;
                        MoveFigurRight(_player.X);
                        _stepsleft = 0;
                    }
                    else                                //Ändern der Richtung
                    {
                        //restlichen schritte
                        _stepsleft = counter - (7 - _player.X);
                        _player.X = 7;
                        _spalte = _player.X;
                        MoveFigurRight(_player.X);
                        _player.CurrentState = State.Up;
                    }
                    break;

                case State.Left:
                    if (_player.X - counter > 0)
                    {
                        _player.X -= counter;
                        MoveFigurLeft(_player.X);
                        _spalte = _player.X;
                        _stepsleft = 0;
                    }
                    else                                //bei Änderung der Richtung
                    {
                        if (_zeile != 0)
                        {
                            //restlichen Schritte
                            _stepsleft = counter - _player.X;
                            _player.X = 0;
                            MoveFigurLeft(_player.X);
                            _spalte = _player.X;
                            _player.CurrentState = State.Up;
                        }
                        else                             //Ziel angekommen
                        {
                            if (Data.WiederholungSem1 || Data.WiederholungSem2 || Data.WiederholungSem3 || Data.WiederholungSem4 || Data.WiederholungSem5)
                            {
                                ReplaceFigur(1, _zeile);
                                _stepsleft = 0;
                                MessageBox.Show("Es sind noch Prüfungen offen!\rBitte begib dich zu den Nachprüfungen.", "Offene Prüfungen", MessageBoxButton.OK, MessageBoxImage.Warning);
                            }
                            else
                            {
                                _zeile = 0;
                                _spalte = 0;
                                ReplaceFigur(_spalte, _zeile);
                                Fertsch = true;
                                _stepsleft = 0;

                                Prüfung();                  //Abschlussprüfung
                                //TODO Schönes Bildchen zum Schluss
                                Close();
                            }
                        }
                    }
                    break;
            }
        }

        /// <summary>
        /// Prüfungsereignis steht an, wenn man auf ein Prüfungsfeld kommt
        /// </summary>
        private void Prüfung()
        {
            Prüfungsbogen _exam = new Prüfungsbogen(Data.AktuellesSemester, Data.VorkursHinweis);
            _exam.ShowDialog();

            if (_exam.HintUsed)
                Data.VorkursHinweis = false;

            bool _ergebnis = _exam.Ergebnis;
            Semester(Data.AktuellesSemester, _ergebnis);

            //wenn Prüfung bestanden Semester hochzählen und Lebensanzeige anpassen
            Data.AktuellesSemester++;
            Data.Lebensanzeige -= Data.Firma.StressFaktor * 4;
            LblSemester.Content = Data.AktuellesSemester + ". Semester";
        }

        /// <summary>
        /// Überprüfen, wenn ein Semester bestanden wurde
        /// </summary>
        /// <param name="semester">zu prüfende Semester</param>
        /// <param name="ergebnis">bestanden oder nicht</param>
        private void Semester(int semester, bool ergebnis)
        {
            switch (semester)
            {
                case 1:
                    Data.Semester1 = ergebnis;
                    Data.WiederholungSem1 = !ergebnis;
                    break;
                case 2:
                    Data.Semester2 = ergebnis;
                    Data.WiederholungSem2 = !ergebnis;
                    break;
                case 3:
                    Data.Semester3 = ergebnis;
                    Data.WiederholungSem3 = !ergebnis;
                    break;
                case 4:
                    Data.Semester4 = ergebnis;
                    Data.WiederholungSem4 = !ergebnis;
                    break;
                case 5:
                    Data.Semester5 = ergebnis;
                    Data.WiederholungSem5 = !ergebnis;
                    break;
                case 6:
                    if (ergebnis)
                        BachelorBestanden();
                    else
                        //TODO Wiederholungsprüfung
                        Prüfung();

                    break;
            }
        }

        private void BachelorBestanden()
        {
            MessageBox.Show("Bestanden");
            Close();
        }

        /// <summary>
        /// Figur nach rechts bewegen in der selben Zeile
        /// </summary>
        /// <param name="newColumn">neu Spalte</param>
        private void MoveFigurRight(int newColumn)
        {
            for (int _i = _spalte; _i <= newColumn; _i++)
            {
                ReplaceFigur(_i, _zeile);
            }
        }

        /// <summary>
        /// Figur nach oben bewegen in der selben Spalte
        /// </summary>
        /// <param name="newRow">neue Zeile</param>
        private void MoveFigurUp(int newRow)
        {
            for (int _i = _zeile; _i >= newRow; _i--)
            {
                ReplaceFigur(_spalte, _i);
            }
        }

        /// <summary>
        /// Figur nach links bewegen in der selben Zeile
        /// </summary>
        /// <param name="newColumn">neue Spalte</param>
        private void MoveFigurLeft(int newColumn)
        {
            for (int _i = _spalte; _i >= newColumn; _i--)
            {
                ReplaceFigur(_i, _zeile);
            }
        }

        /// <summary>
        /// neue Position der Figur festlegen
        /// </summary>
        /// <param name="spalte">neue Spalte</param>
        /// <param name="zeile">neue Zeile</param>
        private void ReplaceFigur(int spalte, int zeile)
        {
            Grid.SetColumn(Rec, spalte);
            Grid.SetRow(Rec, zeile);

            MyGrid.UpdateLayout();
        }

        private void BtnNachpruefung1_Click(object sender, RoutedEventArgs e)
        {
            Prüfungsbogen _exam = new Prüfungsbogen(1, Data.VorkursHinweis);
            _exam.ShowDialog();

            if (_exam.HintUsed)
                Data.VorkursHinweis = false;

            bool _ergebnis = _exam.Ergebnis;
            Semester(1, _ergebnis);

            Data.Lebensanzeige -= Data.Firma.StressFaktor * 2;
        }

        private void BtnNachpruefung2_Click(object sender, RoutedEventArgs e)
        {
            Prüfungsbogen _exam = new Prüfungsbogen(2, Data.VorkursHinweis);
            _exam.ShowDialog();

            if (_exam.HintUsed)
                Data.VorkursHinweis = false;

            bool _ergebnis = _exam.Ergebnis;
            Semester(2, _ergebnis);

            Data.Lebensanzeige -= Data.Firma.StressFaktor * 2;
        }

        private void BtnNachpruefung3_Click(object sender, RoutedEventArgs e)
        {
            Prüfungsbogen _exam = new Prüfungsbogen(3, Data.VorkursHinweis);
            _exam.ShowDialog();

            if (_exam.HintUsed)
                Data.VorkursHinweis = false;

            bool _ergebnis = _exam.Ergebnis;
            Semester(3, _ergebnis);

            Data.Lebensanzeige -= Data.Firma.StressFaktor * 2;
        }

        private void BtnNachpruefung4_Click(object sender, RoutedEventArgs e)
        {
            Prüfungsbogen _exam = new Prüfungsbogen(4, Data.VorkursHinweis);
            _exam.ShowDialog();

            if (_exam.HintUsed)
                Data.VorkursHinweis = false;

            bool _ergebnis = _exam.Ergebnis;
            Semester(4, _ergebnis);

            Data.Lebensanzeige -= Data.Firma.StressFaktor * 2;
        }

        private void BtnNachpruefung5_Click(object sender, RoutedEventArgs e)
        {
            Prüfungsbogen _exam = new Prüfungsbogen(5, Data.VorkursHinweis);
            _exam.ShowDialog();

            if (_exam.HintUsed)
                Data.VorkursHinweis = false;

            bool _ergebnis = _exam.Ergebnis;
            Semester(5, _ergebnis);

            Data.Lebensanzeige -= Data.Firma.StressFaktor * 2;
        }

        private void BtnNachpruefung6_Click(object sender, RoutedEventArgs e)
        {
            Prüfungsbogen _exam = new Prüfungsbogen(6, Data.VorkursHinweis);
            _exam.ShowDialog();

            if (_exam.HintUsed)
                Data.VorkursHinweis = false;

            bool _ergebnis = _exam.Ergebnis;
            Semester(6, _ergebnis);

            Data.Lebensanzeige -= Data.Firma.StressFaktor * 2;
        }

        void AllowUIToUpdate()
        {
            DispatcherFrame _frame = new DispatcherFrame();

            Dispatcher.CurrentDispatcher.BeginInvoke(DispatcherPriority.ContextIdle, new DispatcherOperationCallback(delegate
            {
                _frame.Continue = false;

                return null;

            }), null);

            Dispatcher.PushFrame(_frame);
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!Fertsch && MessageBox.Show("Willst du dein Studium WIRKLICH abbrechen?", "BA-Spiel", MessageBoxButton.YesNo, MessageBoxImage.Warning) != MessageBoxResult.Yes)
            {
                e.Cancel = true;
            }
        }

        private void BtnLebensanzeige_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Die Lebensanzeige entspricht deiner Gesundheit.\nSie ist abhängig von deinem Stresslevel, deiner Zufriedenheit und deinem Lohn.\n\nVersuche sie nicht auf 0% zu bringen!", "Lebensanzeige");
        }
    }

    //enum zur Definition der Bewegungsrichtung
    public enum State { Up, Right, Left }
    //Klasse die Position und Richtung des Spieler/der Figur angibt
    public class Spieler
    {
        public State CurrentState = State.Right;
        public int X;
        public int Y;
    }
}
