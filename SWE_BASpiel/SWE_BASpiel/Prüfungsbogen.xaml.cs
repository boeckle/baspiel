﻿using System;
using System.Windows;
using System.Collections;

namespace SWE_BASpiel
{
    /// <summary>
    /// Interaktionslogik für Prüfungsbogen.xaml
    /// </summary>
    public partial class Prüfungsbogen : Window
    {
        private int _semester, punkte, anzFragen, aktFrage;
        private Data_Pruefung pruefData;
        private ArrayList aufgaben;
        public bool Ergebnis;
        private bool abbrechen, firstInit=true;

        public Prüfungsbogen(int semester, bool vorkursHinweis)
        {
            InitializeComponent();
            aktFrage = 0;
            _semester = semester;
            aufgaben = new ArrayList();
            pruefData = new Data_Pruefung();
            pruefData.init();

            if (!vorkursHinweis)
                BtnHint.Visibility = Visibility.Hidden;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Task t = (Task)aufgaben[aktFrage];
            MessageBox.Show("Antwortmöglichkeit " + t.Richtig.ToString() + " ist die richtige Antwort.", "Hinweise", MessageBoxButton.OK, MessageBoxImage.Information);
            HintUsed = true;
            BtnHint.Visibility = Visibility.Hidden;
        }

        private void btn_weiter_Click(object sender, RoutedEventArgs e)
        {
            //erst schauen, ob irgendein Radiobutton gechecked ist
            if (rb_AntwortA.IsChecked == true || rb_AntwortB.IsChecked == true || rb_AntwortC.IsChecked == true)
            {
                // Ergebnis der vorhergehenden Frage ermitteln
                Task t = (Task)aufgaben[aktFrage];
                if ((rb_AntwortA.IsChecked == true) && (t.Richtig == 1))
                {
                    punkte++;
                }
                if ((rb_AntwortB.IsChecked == true) && (t.Richtig == 2))
                {
                    punkte++;
                }
                if ((rb_AntwortC.IsChecked == true) && (t.Richtig == 3))
                {
                    punkte++;
                }

                if (aktFrage < (anzFragen - 1))
                {
                    // nächste Frage einblenden
                    aktFrage++;
                    Task tNeu = (Task)aufgaben[aktFrage];

                    //neue Antworten holen
                    tb_Aufgabe.Text = tNeu.Frage;
                    rb_AntwortA.Content = tNeu.Antwort_1;
                    rb_AntwortB.Content = tNeu.Antwort_2;
                    rb_AntwortC.Content = tNeu.Antwort_3;
                    //Radiobuttons unchecken
                    rb_AntwortA.IsChecked = false;
                    rb_AntwortB.IsChecked = false;
                    rb_AntwortC.IsChecked = false;
                }
                else
                {
                    
                    if ((anzFragen-punkte) > (anzFragen/2)) // nicht bestanden
                    {
                        Ergebnis = false;
                        abbrechen = true;
                        MessageBox.Show("Du hast die Prüfung nicht bestanden!", "Prüfungsergebnis", MessageBoxButton.OK, MessageBoxImage.Warning);
                    }
                    // Prüfung wurde bestanden
                    else
                    {
                        Ergebnis = true;
                        abbrechen = true;
                        MessageBox.Show("Herzlichen Glückwunsch! Du hast die Prüfung bestanden.", "Prüfungsergebnis", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    // Prüfungsfenster schließen
                    Close();
                }
            }
        }

        /// <summary>
        /// Wird ausgeführt, wenn das Fenster geöffnet wird. Oberfläche aktualisieren
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_Activated(object sender, EventArgs e)
        {
            if (firstInit)
            {
                LblBezeichnung.Content = "Prüfung " + _semester + ". Semester";
                switch (_semester)
                {
                    case 1:     // Prüfung 1. Semester
                        aufgaben = pruefData.Sem1Prüfung.Aufgaben;
                        anzFragen = aufgaben.Count;
                        break;

                    case 2:     // Prüfung 2. Semester
                        aufgaben = pruefData.Sem2Prüfung.Aufgaben;
                        anzFragen = aufgaben.Count;
                        break;

                    case 3:     // Prüfung 3. Semester
                        aufgaben = pruefData.Sem3Prüfung.Aufgaben;
                        anzFragen = aufgaben.Count;
                        break;

                    case 4:     // Prüfung 4. Semester
                        aufgaben = pruefData.Sem4Prüfung.Aufgaben;
                        anzFragen = aufgaben.Count;
                        break;

                    case 5:      // Prüfung 5. Semester
                        aufgaben = pruefData.Sem5Prüfung.Aufgaben;
                        anzFragen = aufgaben.Count;
                        break;

                    case 6:     // Prüfung 6. Semester
                        LblBezeichnung.Content = "Abschlussprüfung";
                        aufgaben = pruefData.Sem6Prüfung.Aufgaben;
                        anzFragen = aufgaben.Count;
                        break;
                }


                // Anzeigen der ersten Aufgabe und deren Lösungsmöglichkeiten
                Task t = (Task)aufgaben[0];

                tb_Aufgabe.Text = t.Frage;
                rb_AntwortA.Content = t.Antwort_1;
                rb_AntwortB.Content = t.Antwort_2;
                rb_AntwortC.Content = t.Antwort_3;

                firstInit = false;
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!abbrechen && MessageBox.Show("Möchtest du die Prüfung wirklich abbrechen?", "Prüfung", MessageBoxButton.YesNo, MessageBoxImage.Warning) != MessageBoxResult.Yes)
            {
                Ergebnis = false;
                e.Cancel = true;
            }
        }

        public bool HintUsed { get; set; }
    }
}
