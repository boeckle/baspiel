﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace SWE_BASpiel
{
    class Data_Pruefung
    {
        ArrayList fragen = new ArrayList();

        Task frage1 = new Task("Gegeben ist die Zahl 1010 im Dualsystem. Wie heißt die Zahl im Dezimalsystem?",
                              "10", "8", "9", 1);

        Task frage2 = new Task("Was bedeutet 'Open-Source'?",
                              "Man kann die Software überall verwenden", "Alles ist kostenlos", "Der Quellcode ist zugänglich und änderbar", 3);

        Task frage3 = new Task("Wie heißt die Technik, bei der jeder vernetzte Rechner sowohl als Server, als auch als Client fungieren kann und die einzelnen Rechner direkt miteinander kommunizieren?",
                              "Multifunctional Networking", "Peer-to-Peer", "Computer-to-Computer", 2);

        Task frage4 = new Task("Aus wievielen Bits besteht ein Byte?",
                              "6", "7", "8", 3);

        Task frage5 = new Task("Was ist HTML?",
                              "Eine Computerprogrammiersprache", "Eine Auszeichnungssprache zur Gestaltung von Webseiten", "Ein Komprimierungsformat für Datein", 2);

        Task frage6 = new Task("Welche Zahl hat das Hexadezimalsystem als Basis?",
                              "8", "3", "16", 3);

        Task frage7 = new Task("Was ergibt diese ODER-Verknüpfung: 1 v 0?",
                              "1", "0", "2", 1);

        Task frage8 = new Task("Wieviele Nachkommastellen hat eine Integerzahl?",
                              "0", "5", "113498", 1);

        Task frage9 = new Task("Ein Rechner besteht im Prinzip aus CPU, Speicher und...",
                              "Prozessor", "RAM", "Ein- und Ausgabeeinheiten", 3);

        Task frage10 = new Task("Der Ursprung des Internets liegt im Jahr 1969 in der Entwicklung eines Netzwerks names",
                              "INTERWEB", "ARPANET", "WIDENET", 2);

        Task frage11 = new Task("Um Wechselspannung in Gleichspannung umzusetzen nutzt man:",
                              "Dioden", "Transistoren", "Kondensatoren", 1);

        Task frage12 = new Task("Die Bewegung von Ladungsträgern wird auch ... genannt",
                              "Induktion", "Potential", "Strom", 3);

        Task frage13 = new Task("Worfür steht die Abkürzung LAN?",
                              "Lancom Account Number", "Local Area Network", "Local Adress Network", 2);

        Task frage14 = new Task("Wie heißt die Hardwareadresse eines PCs?",
                      "Proxy-Adresse", "IP-Adresse", "MAC-Adresse", 3);

        Task frage15 = new Task("Wofür steht die Abkürzung BIOS?",
                      "Basic-Input-Output-System", "Board-Integrated-Operating-System", "Board-Input-Output-System", 1);

        Task frage16 = new Task("Welches Windows gibt es nicht?",
                      "Windows 95", "Windows 98", "Windows 2001", 3);

        Task frage17 = new Task("Für welche Programmiersprache ist folgendes Statement üblich? '#include <stdio.h>'",
                      "Java", "PHP", "C", 3);

        Task frage18 = new Task("Wann wurde Konrad Zuses Z1 gebaut?",
                      "1937", "1949", "1964", 1);

        Task frage19 = new Task("Wofür steht CSS?",
                      "Cascading Stylesheets", "Complex Stylesheets", "Coloured Site Standard", 1);

        Task frage20 = new Task("Wie lautet die Gleichung richtig?",
                      "U=R*I", "I=R/U", "R=I/U", 1);

        Pruefung Sem1Pruefung = new Pruefung("Prüfung 1. Semester");
        Pruefung Sem2Pruefung = new Pruefung("Prüfung 2. Semester");
        Pruefung Sem3Pruefung = new Pruefung("Prüfung 3. Semester");
        Pruefung Sem4Pruefung = new Pruefung("Prüfung 4. Semester");
        Pruefung Sem5Pruefung = new Pruefung("Prüfung 5. Semester");
        Pruefung Sem6Pruefung = new Pruefung("Abschlussprüfung 6. Semester");

        public void init() //muss unbedingt ausgeführt werden, wenn eine Instanz der Klasse Data_pruefung angelegt wird
        {
            fragen.Add(frage1);
            fragen.Add(frage2);
            fragen.Add(frage3);
            fragen.Add(frage4);
            fragen.Add(frage5);
            fragen.Add(frage6);
            fragen.Add(frage7);
            fragen.Add(frage8);
            fragen.Add(frage9);
            fragen.Add(frage10);
            fragen.Add(frage11);
            fragen.Add(frage12);
            fragen.Add(frage13);
            fragen.Add(frage14);
            fragen.Add(frage15);
            fragen.Add(frage16);
            fragen.Add(frage17);
            fragen.Add(frage18);
            fragen.Add(frage19);
            fragen.Add(frage20);
            Sem1Pruefung.addTask(fragen.GetRange(0, 3));
            Sem2Pruefung.addTask(fragen.GetRange(3, 3));
            Sem3Pruefung.addTask(fragen.GetRange(6, 3));
            Sem4Pruefung.addTask(fragen.GetRange(9, 3));
            Sem5Pruefung.addTask(fragen.GetRange(12, 3));
            Sem6Pruefung.addTask(fragen.GetRange(15, 5));
        }

        public Pruefung Sem1Prüfung
        {
            get { return Sem1Pruefung; }
        }
        public Pruefung Sem2Prüfung
        {
            get { return Sem2Pruefung; }
        }
        public Pruefung Sem3Prüfung
        {
            get { return Sem3Pruefung; }
        }
        public Pruefung Sem4Prüfung
        {
            get { return Sem4Pruefung; }
        }
        public Pruefung Sem5Prüfung
        {
            get { return Sem5Pruefung; }
        }
        public Pruefung Sem6Prüfung
        {
            get { return Sem6Pruefung; }
        }
    }
}
