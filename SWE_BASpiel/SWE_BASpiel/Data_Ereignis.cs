﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace SWE_BASpiel
{
    class Data_Ereignis
    {
        public Ereignis[,] EreignisArray = new Ereignis[11, 8];

        public void init()
        {
            //  1. Semester
            EreignisArray[10, 1] = _ereignis1;
            EreignisArray[10, 2] = _ereignis2;
            EreignisArray[10, 3] = _ereignis3;
            EreignisArray[10, 5] = _ereignis4;
            EreignisArray[10, 6] = _ereignis5;
            EreignisArray[10, 7] = _ereignis6;
            //  2. Semester
            EreignisArray[8, 7] = _ereignis7;
            EreignisArray[8, 5] = _ereignis8;
            EreignisArray[8, 4] = _ereignis9;
            EreignisArray[8, 3] = _ereignis10;
            EreignisArray[8, 2] = _ereignis11;
            EreignisArray[8, 1] = _ereignis12;
            EreignisArray[8, 0] = _ereignis13;
            //  3. Semester
            EreignisArray[6, 0] = _ereignis14;
            EreignisArray[6, 1] = _ereignis15;
            EreignisArray[6, 3] = _ereignis16;
            EreignisArray[6, 4] = _ereignis17;
            EreignisArray[6, 5] = _ereignis18;
            EreignisArray[6, 6] = _ereignis19;
            EreignisArray[6, 7] = _ereignis20;
            //  4. Semester
            EreignisArray[4, 6] = _ereignis21;
            EreignisArray[4, 5] = _ereignis22;
            EreignisArray[4, 4] = _ereignis23;
            EreignisArray[4, 3] = _ereignis24;
            EreignisArray[4, 2] = _ereignis25;
            EreignisArray[4, 1] = _ereignis26;
            EreignisArray[4, 0] = _ereignis27;
            //  5. Semester
            EreignisArray[2, 0] = _ereignis28;
            EreignisArray[2, 1] = _ereignis29;
            EreignisArray[2, 2] = _ereignis30;
            EreignisArray[2, 3] = _ereignis31;
            EreignisArray[2, 4] = _ereignis32;
            EreignisArray[2, 6] = _ereignis33;
            EreignisArray[2, 7] = _ereignis34;
            //  6. Semester
            EreignisArray[0, 7] = _ereignis35;
            EreignisArray[0, 6] = _ereignis36;
            EreignisArray[0, 5] = _ereignis37;
            EreignisArray[0, 4] = _ereignis38;
            EreignisArray[0, 3] = _ereignis39;
            EreignisArray[0, 2] = _ereignis40;
        }

        //---------- 1. Semester
        Ereignis _ereignis1 = new Ereignis("Die erste Woche ist sehr entspannt.\nGenieße die freie Zeit.", 1);
        Ereignis _ereignis2 = new Ereignis("Kater nach einem langen Abend im Jugendclub.", -1);
        Ereignis _ereignis3 = new Ereignis("Zahltag!\nDir wurde dein Lohn überwiesen.", 0);
        Ereignis _ereignis4 = new Ereignis("Elektrotechnik-Aufgaben rauben dir den letzten Nerv.", -1);
        Ereignis _ereignis5 = new Ereignis("Prof. Friedrich will die Mathe zu deinem besten Freund machen.\nDavon bist du nicht so begeistert wie er.", -1);
        Ereignis _ereignis6 = new Ereignis("Zahltag!\nDir wurde dein Lohn überwiesen.", 0);

        //---------- 2. Semester
        Ereignis _ereignis7 = new Ereignis("Du verschläfst die erste Einheit.\nAuch wenn es gut für dich war, pflegt es dein Verhältnis zum Dozenten nicht.", -1);
        Ereignis _ereignis8 = new Ereignis("Zahltag!\nDir wurde dein Lohn überwiesen.", 0);
        Ereignis _ereignis9 = new Ereignis("Elektrotechnik-Aufgaben rauben dir den letzten Nerv.", -1);
        Ereignis _ereignis10 = new Ereignis("Prof. Lehnguth erwischt dich beim Handy spielen.", -1);
        Ereignis _ereignis11 = new Ereignis("Kater nach einem langen Abend im Jugendclub.", -1);
        Ereignis _ereignis12 = new Ereignis("Dein Dozent macht Prüfungsvorbereitung mit dir.", 1);
        Ereignis _ereignis13 = new Ereignis("Zahltag!\nDir wurde dein Lohn überwiesen.", 0);

        //---------- 3. Semester
        Ereignis _ereignis14 = new Ereignis("Zahltag!\nDir wurde dein Lohn überwiesen.", 0);
        Ereignis _ereignis15 = new Ereignis("Heute hast du keine Vorlesungen.", 1);
        Ereignis _ereignis16 = new Ereignis("Kater nach einem langen Abend im Jugendclub.", -1);
        Ereignis _ereignis17 = new Ereignis("Du findest keinen Parkplatz und kommst zu spät zur Uni.", -1);
        Ereignis _ereignis18 = new Ereignis("Der Jugendclub ist verwüsstet.\nDeinen Nachmittag wirst du wohl mit aufräumen verbringen.", -1);
        Ereignis _ereignis19 = new Ereignis("Zahltag!\nDir wurde dein Lohn überwiesen.", 0);
        Ereignis _ereignis20 = new Ereignis("Heute 5 Einheiten. Gönn dir!", -1);

        //---------- 4. Semester
        Ereignis _ereignis21 = new Ereignis("Autsch. Du bist auf dem Weg zur Uni im Schnee ausgerutscht.", -1);
        Ereignis _ereignis22 = new Ereignis("Prof. Lehnguth spendiert dir einen Kaffee.", 1);
        Ereignis _ereignis23 = new Ereignis("Zahltag!\nDir wurde dein Lohn überwiesen.", 0);
        Ereignis _ereignis24 = new Ereignis("Kater nach einem langen Abend im Jugendclub.", -1);
        Ereignis _ereignis25 = new Ereignis("Du hilfst deinen Kommilitonen beim Lernen\nund wirst mit viel Bier belohnt.", 1);
        Ereignis _ereignis26 = new Ereignis("Strafzettel fürs Falschparken.", -1);
        Ereignis _ereignis27 = new Ereignis("Zahltag!\nDir wurde dein Lohn überwiesen.", 0);

        //---------- 5. Semester
        Ereignis _ereignis28 = new Ereignis("Zahltag!\nDir wurde dein Lohn überwiesen.", 0);
        Ereignis _ereignis29 = new Ereignis("Du verpeilst den Abgabetermin deiner Praxisarbeit.", -1);
        Ereignis _ereignis30 = new Ereignis("Jemand hat dein Auto auf dem Parkplatz beschädigt.", -1);
        Ereignis _ereignis31 = new Ereignis("Kater nach einem langen Abend im Jugendclub.", -1);
        Ereignis _ereignis32 = new Ereignis("Zahltag!\nDir wurde dein Lohn überwiesen.", 0);
        Ereignis _ereignis33 = new Ereignis("Heute 5 Blöcke. Gönn dir!", -1);
        Ereignis _ereignis34 = new Ereignis("Dein Dozent macht Prüfungsvorbereitung mit dir.", 1);

        //---------- 6. Semester
        Ereignis _ereignis35 = new Ereignis("Kater nach einem langen Abend im Jugendclub.", -1);
        Ereignis _ereignis36 = new Ereignis("Zahltag!\nDir wurde dein Lohn überwiesen.", 0);
        Ereignis _ereignis37 = new Ereignis("Du hast vergessen, dass du Ausfall hast\n und bist eine Stunde zu früh in der Uni.", -1);
        Ereignis _ereignis38 = new Ereignis("Zahltag!\nDir wurde dein Lohn überwiesen.", 0);
        Ereignis _ereignis39 = new Ereignis("Du gewinnst bei einem Bier-Pong-Turnier.", 1);
        Ereignis _ereignis40 = new Ereignis("Dein Fahrrad wurde geklaut.", -1);
    }
}
