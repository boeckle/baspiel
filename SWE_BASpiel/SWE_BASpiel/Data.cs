﻿using System.ComponentModel;

namespace SWE_BASpiel
{
    public class Data : INotifyPropertyChanged
    {
        // Event bei Änderung, damit bekommt die GUI die Änderung mit
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, e);
        }

        private bool _semester1;
        private bool _semester2;
        private bool _semester3;
        private bool _semester4;
        private bool _semester5;
        private bool _semester6;
        private int _lebensanzeige;

        private bool _wdhSem1, _wdhSem2, _wdhSem3, _wdhSem4, _wdhSem5, _wdhSem6;

        public string Name { get; set; }
        public string Vorname { get; set; }
        public Company Firma { get; set; }
        public bool Vorkurs { get; set; }
        public bool VorkursHinweis { get; set; }
        public int AktuellesSemester { get; set; }

        public bool Semester1
        {
            get { return _semester1; }
            set
            {
                _semester1 = value;
                OnPropertyChanged(new PropertyChangedEventArgs("Semester1"));
            }
        }

        public bool Semester2
        {
            get { return _semester2; }
            set
            {
                _semester2 = value;
                OnPropertyChanged(new PropertyChangedEventArgs("Semester2"));
            }
        }

        public bool Semester3
        {
            get { return _semester3; }
            set
            {
                _semester3 = value;
                OnPropertyChanged(new PropertyChangedEventArgs("Semester3"));
            }
        }

        public bool Semester4
        {
            get { return _semester4; }
            set
            {
                _semester4 = value;
                OnPropertyChanged(new PropertyChangedEventArgs("Semester4"));
            }
        }

        public bool Semester5
        {
            get { return _semester5; }
            set
            {
                _semester5 = value;
                OnPropertyChanged(new PropertyChangedEventArgs("Semester5"));
            }
        }

        public bool Semester6
        {
            get { return _semester6; }
            set
            {
                _semester6 = value;
                OnPropertyChanged(new PropertyChangedEventArgs("Semester6"));
            }
        }

        public int Lebensanzeige
        {
            get { return _lebensanzeige; }
            set
            {
                _lebensanzeige = value;
                OnPropertyChanged(new PropertyChangedEventArgs("Lebensanzeige"));
            }
        }

        public bool WiederholungSem1
        {
            get { return _wdhSem1; }
            set
            {
                _wdhSem1 = value;
                OnPropertyChanged(new PropertyChangedEventArgs("WiederholungSem1"));
            }
        }
        public bool WiederholungSem2
        {
            get { return _wdhSem2; }
            set
            {
                _wdhSem2 = value;
                OnPropertyChanged(new PropertyChangedEventArgs("WiederholungSem2"));
            }
        }
        public bool WiederholungSem3
        {
            get { return _wdhSem3; }
            set
            {
                _wdhSem3 = value;
                OnPropertyChanged(new PropertyChangedEventArgs("WiederholungSem3"));
            }
        }
        public bool WiederholungSem4
        {
            get { return _wdhSem4; }
            set
            {
                _wdhSem4 = value;
                OnPropertyChanged(new PropertyChangedEventArgs("WiederholungSem4"));
            }
        }
        public bool WiederholungSem5
        {
            get { return _wdhSem5; }
            set
            {
                _wdhSem5 = value;
                OnPropertyChanged(new PropertyChangedEventArgs("WiederholungSem5"));
            }
        }
        public bool WiederholungSem6
        {
            get { return _wdhSem6; }
            set
            {
                _wdhSem6 = value;
                OnPropertyChanged(new PropertyChangedEventArgs("WiederholungSem6"));
            }
        }
    }

    public class Company
    {
        public string Name { get; set; }
        public int LohnFaktor { get; set; }
        public int StressFaktor { get; set; }
    }
}
